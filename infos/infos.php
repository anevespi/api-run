<!DOCTYPE html>
<html>
    <head>
        <title>Infos serveur</title>
        <meta charset="utf-8"/>
    </head>
    <body>
    <h1>Infos serveur</h1>
    <table border="1">
        <tr><th>Nom utilisateur</th><th>Distribution</th><th>Version Linux</th><th>Taille RAM</th><th>Taille disque dur</th></tr>
       
	 <tr>
		<td><?php echo get_current_user(); ?></td>
		<td><?php echo php_uname('s'); ?></td>
		<td><?php echo php_uname('r'); ?></td>
		<td><?php echo memory_get_usage(); ?></td>
		<td><?php echo disk_free_space("/") . ' / ' . disk_total_space("/"); ?></d>
	</tr> 
    </table>
    </body>
</html>
