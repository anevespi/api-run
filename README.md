Etat du projet:
 - 1 site conseils.codesavethequeen.picagraine.net en html qui fonctionne quand je lance dans le container (avec docker compose up)
 - 1 site infos.codesavethequeen.picagraine.net en php qui ne fonctionne pas (erreur 403 forbidden)
 - le 3ème site "commandes" est écrit en php mais pas configuré dans docker-compose.yml ni nginx.conf

Structure des dossier:
Dans le repo git on a tous les dossiers nécessaires:
- commandes qui contient le fichier php du site commandes 
- conseils qui contient le fichier html du site conseils
- infos qui contient le fichier php du site infos
- nginx avec le fichier de configuration nginx.conf
- un fichier docker-compose.yml 

Pour lancer le projet: (serveur 103)

--> faire docker compose up
--> ouvrir un navigateur et taper conseils.codesavethequeen.picagraine.net et infos.codesavethequeen.picagraine.net
